#pragma once

#include "akt/ptk/links.h"
#include "akt/ptk/thread.h"

namespace akt {
  namespace ptk {
    class Semaphore {
      unsigned count;
      DQueue<Thread> waiting;

    public:
      Semaphore(unsigned count);

      void acquire();
      void release();
    };
  }
}
