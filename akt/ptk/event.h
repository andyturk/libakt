#pragma once

#include "akt/ptk/kernel.h"
#include "akt/ptk/thread.h"

namespace akt {
  namespace ptk {
    typedef int32_t eventmask_t;

    class Event {
      friend class Kernel;

      DQueue<Thread> waiting;
    public:
      Event() : waiting(&Thread::ready_link) {}
    };
  }
}
