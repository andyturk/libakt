#include <iostream>
#include "akt/ptk/ptk.h"

using namespace akt::ptk;

unsigned clock_value;

Kernel kernel;

class Blink : public Thread {
public:
  const unsigned period;

  Blink(Kernel &kernel, const char *name, unsigned period) :
    Thread(kernel, name),
    period(period)
  {}

  void set_state(bool value) {
    std::cout << "at time " << clock_value << ' ' << name << " is "
              << (value ? "on" : "off") << std::endl;
  }

  void run() {
    PTK_BEGIN();

    while (1) {
      set_state(true);
      PTK_SLEEP(period);
      set_state(false);
      PTK_SLEEP(period);
    }

    PTK_END();
  }
} blink(kernel, "blink", 2);

int main(int argc, char *argv[]) {
  kernel.lock();
  kernel.schedule(blink);
  kernel.unlock();

  for (clock_value=0; clock_value < 10; clock_value += 1) {
    kernel.run_once();

    // simulate RTC interrupt. One unit of time passes each iteration
    kernel.enter_isr();
    kernel.expire_timers(1);
    kernel.leave_isr();
  }

  return 0;
}
