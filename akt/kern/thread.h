#pragma once

#include <akt/kern/list.h>

#include <cstdint>
#include <cstddef>
#include <new>

namespace akt {
  namespace kern {
    typedef uint64_t stack_alignment_t;

    template<typename T, std::size_t N>
    constexpr std::size_t countof(T const (&)[N]) noexcept {
      return N;
    }
    
    constexpr char *align_down(char *addr, unsigned size) {
      return addr - ((unsigned) addr) % size;
    }

    constexpr char *align_up(char *addr, unsigned size) {
      return addr + (size - (reinterpret_cast<unsigned>(addr) % size));
    }

    enum thread_privilege_t { PRIVILEGED_THREADS, UNPRIVILEGED_THREADS };

    // whether an exception return will resume threaded code or another exception
    enum exc_return_mode_t { EXC_THREAD, EXC_HANDLER };

    // whether an exception return should use the Main or Process stack pointer
    enum exc_stack_type_t { EXC_MSP = 0, EXC_PSP = 2 };

    // whether a hardware stacked exception includes floating point registers
    enum exc_frame_type_t { EXC_BASIC = 0, EXC_FP = 4 };

    static constexpr uint16_t* exc_return(exc_stack_type_t stack, exc_return_mode_t ret, exc_frame_type_t frame) {
      return reinterpret_cast<uint16_t*>(0xfeffffe1 | (frame == EXC_BASIC ? 0x00000010 : 0) |
                                         (ret == EXC_THREAD ? 0x00000008 : 0) | (stack == EXC_PSP ? 0x00000004 : 0));
    }

    static constexpr uint32_t control_value(thread_privilege_t privilege, exc_stack_type_t stack, bool fp_active) {
      return (privilege == UNPRIVILEGED_THREADS ? 0x01 : 0) | (stack == EXC_PSP ? 0x02 : 0) | (fp_active ? 0x04 : 0);
    }

    enum xpsr_bits : uint32_t {
      ISR_MASK  = 0x000001ff,
      ISR_SHIFT = 0,
      THUMB_BIT = 0x01000000,
    };

    struct CortexExceptionFrame {
      CortexExceptionFrame(void (*fn)(void* arg), void* arg, void (*continuation)(void *arg))
          : r0(reinterpret_cast<uint32_t>(arg))
          , lr(reinterpret_cast<uint16_t*>(continuation))
          , pc(reinterpret_cast<uint16_t*>(fn))
          , xpsr(THUMB_BIT) {}

      uint32_t r0;
      uint32_t r1;
      uint32_t r2;
      uint32_t r3;
      uint32_t r12;  // intra prcedure-call scratch register
      uint16_t* lr;  // link register (return address)
      uint16_t* pc;  // program counter
      uint32_t xpsr; // program status
    } __attribute__((aligned(8)));

    struct ContextSwitchFrame {
      ContextSwitchFrame(void (*fn)(void* arg), void* arg, void (*continuation)(void* arg))
          : control(control_value(UNPRIVILEGED_THREADS, EXC_PSP, false))
          , lr(exc_return(EXC_PSP, EXC_THREAD, EXC_BASIC))
          , exception_frame(fn, arg, continuation) {}

      static inline ContextSwitchFrame* __attribute__((always_inline)) save() {
        ContextSwitchFrame* result asm("r0");

        // r4-r11 must be saved according to the Arm Procedure Call Standard (APCS)
        // http://infocenter.arm.com/help/topic/com.arm.doc.ihi0042e/IHI0042E_aapcs.pdf
        //
        // r12 is the intra-procedure call scratch register, and is not saved
        // r2 & r3 are filled with two other values that must be saved with the context
        // r2-r11 (10 registers) can then be saved with a single instruction

        asm volatile("mrs   r0, psp              \n"
                     "mrs   r2, control          \n"
                     "mov   r3, lr               \n"
                     "stmdb r0!, {r2-r11}        \n"
                     : // output
                       "=r"(result)
                     : // input
                     : // clobber
                       "r0", "r2", "r3", "memory");
        return result;
      }

      static inline void __attribute__ ((always_inline)) resume(ContextSwitchFrame *context) {
        // check to see whether the context needs to use MSP or PSP, although only
        // the PSP variant should be active.

        asm volatile("ldmia %[context]!, {r2-r11} \n"
                     "msr psp, %[context]         \n"
                     "msr control, r2             \n"
                     "bx  r3                      \n"
                     : // no outputs
                     : // input
                       [context] "r"(context),
                       [SPSEL] "i" (EXC_PSP)
                     : // clobbered
                       "r2", "r3", "r4", "r5", "r6", "r8", "r9", "r10", "r11"
                     );
      }
      uint32_t control;
      uint16_t* lr;
      uint32_t saved_registers[8];
      CortexExceptionFrame exception_frame;
    } __attribute__ ((aligned (8)));

#if 0
    class KernelContext {
    public:
      typedef void (*kernel_fn)(void *);

      KernelContext(kernel_fn fn, void* arg)
          : control(EXC_PSP)
          , lr(fn)
          , cortex(fn, arg) {}

      static inline KernelContext * __attribute__ ((always_inline)) save() {
        KernelContext *result asm("r0");

        // r4-r11 must be saved according to the Arm Procedure Call Standard (APCS)
        // http://infocenter.arm.com/help/topic/com.arm.doc.ihi0042e/IHI0042E_aapcs.pdf
        //
        // r12 is the intra-procedure call scratch register, and is not saved
        // r2 & r3 are filled with two other values that must be saved with the context
        // r2-r11 can then be saved with a single instruction

        asm volatile("mrs   %[result], psp       \n"
                     "mrs   r2, control          \n"
                     "mov   r3, lr               \n"
                     "stmdb %[result]!, {r2-r11} \n"
                     : // outputs
                       [result] "=r"(result)
                     : // no inputs
                     : // clobbered
                       "r2", "r3", "memory"
                     );
        return result;
      }

      static inline void __attribute__ ((always_inline)) resume(KernelContext *context) {
        // check to see whether the context needs to use MSP or PSP, although only
        // the PSP variant should be active.

        asm volatile("ldmia %[context]!, {r2-r11} \n"
                     "tst r2, %[SPSEL]            \n"
                     "ite eq                      \n"
                     "msreq msp, %[context]       \n"
                     "msrne psp, %[context]       \n"
                     "msr control, r2             \n"
                     "bx  r3                      \n"
                     : // no outputs
                     : // input
                       [context] "r"(context),
                       [SPSEL] "i" (EXC_PSP)
                     : // clobbered
                       "r2", "r3", "r4", "r5", "r6", "r8", "r9", "r10", "r11"
                     );
      }
      uint32_t control;
      uint32_t lr;
      uint32_t r4;
      uint32_t r5;
      uint32_t r6;
      uint32_t r7;
      uint32_t r8;
      uint32_t r9;
      uint32_t r10;
      uint32_t r11;
      CortexExceptionFrame cortex;
    } __attribute__ ((aligned (8)));
#endif
    
    class Kernel;

    class Thread {
      friend class Kernel;

    public:
      enum state_t { NEW, READY, RUNNING, SLEEPING, EXIT };

      Thread(const char *name, char *stack_memory, size_t size)
          : name(name)
          , state(NEW)
          , ticks(0)
          , stack_upper_limit(
                reinterpret_cast<ContextSwitchFrame *>(align_down(stack_memory + size, alignof(ContextSwitchFrame))))
          , stack_lower_limit(reinterpret_cast<uint32_t *>(align_up(stack_memory, sizeof(uint32_t)))) {

        stack = new(stack_upper_limit-1) ContextSwitchFrame(&entry, this, &exit);
      }

      const char *const name;
      state_t get_state() const { return state; }

      //    protected:
      
      virtual void run() {}

      DLink<Thread> registry_link;
      DLink<Thread> ready_link;

      state_t state;
      unsigned ticks;

      ContextSwitchFrame *const stack_upper_limit;
      ContextSwitchFrame *stack;
      volatile uint32_t *const stack_lower_limit;

    private:
      static void entry(void *arg) {
        Thread* thread(reinterpret_cast<Thread *>(arg));
        thread->run();
      }

      static void exit(void *arg) {
        Thread* thread(reinterpret_cast<Thread *>(arg));
        thread->state = EXIT;
      }
    };

    template <unsigned Size>
    class SizedThread : public Thread {
      char storage[Size];

    public:
      SizedThread(const char* name)
          : Thread(name, storage, sizeof(storage)) {}
    };

    class Kernel {
      enum svc_id : uint8_t {
        SVC_SVC1 = 1
      };
      
      static Kernel *the_kernel;

      unsigned idx;
      Thread* threads[4];
      struct {
        Thread* current;
        Thread* next;
      } context;

    public:
      template<unsigned N>
      static uint32_t svc(uint32_t arg1) {
        uint32_t retval;

        asm volatile ("svc %[n] \n"
                      : // output
                        "=r"(retval),
                        "+r"(arg1)
                      : // input
                        [n]"I"(N)
                      : // clobber
                      );
        return retval;
      }
      
      Kernel() : idx(0) {}

      virtual void svc(uint8_t id, CortexExceptionFrame *frame) {
      }

      void do_svc1(int arg1, int arg2, int arg3) {
        asm volatile("svc %[id] \n"
                     : // output
                     : // input
                     [id] "I"(SVC_SVC1),
                       "r"(arg1),
                       "r"(arg2),
                       "r"(arg3)
                     : // clobber
                     );
      }

      void systick_handler() {
        for (unsigned i=1; i < countof(threads); ++i) {
          unsigned probe = (idx + i) % countof(threads);
          if (threads[probe]) {
            context.next = threads[idx = probe];

            // set PendSV
            SCB->ICSR |= SCB_ICSR_PENDSVSET_Msk;
            break;
          }
        }
      }

      void pendsv_handler1() __attribute__((always_inline)) {
        register ContextSwitchFrame* stack asm("r12");

        // According to the Arm Procedure Call Standard (APCS), r4-r11 must be saved
        // on the current thread's stack (assumed here to be the PSP).
        // http://infocenter.arm.com/help/topic/com.arm.doc.ihi0042e/IHI0042E_aapcs.pdf
        //
        // Notes:
        //   This asm block loads r12 with the PSP and pushes r4-r11
        //   r0-r3 and r12 will have already been saved by ARM Cortex-M hardware stacking
        //   r2 & r3 are filled with two other values that must be saved with the context
        //   10 contiguous registers (r2-r11) can be saved with a single instruction

        asm volatile("mrs   %[stack], psp         \n"
                     "mrs   r2, control           \n"
                     "mov   r3, lr                \n"
                     "stmdb %[stack]!, {r2-r11}   \n"
                     : // output
                     [stack] "=r"(stack)
                     : // input
                     : // clobber
                     "r2", "r3", "memory");

        // The next step is to save the updated stack pointer back into the Thread
        // object that's being swapped out.

        asm volatile("ldr r0, %[current]         \n"
                     "str %[stack], [r0]         \n"
                     : // output
                     : // intput
                     [stack] "r"(stack),
                     [current] "m"(the_kernel->context.current->stack)
                     : // clobber
                     "r0", "memory"
                     );

        // Update the kernel state to reflect the new thread to run
        the_kernel->context.current = the_kernel->context.next;

        asm volatile("ldr %[stack], %[next]     \n"
                     "ldmia %[stack], {r2-r11}  \n"
                     "msr psp, %[stack]         \n"
                     "msr control, r2           \n"
                     "bx r3                     \n"
                     : // output
                     : // input
                     [stack] "r"(stack),
                     [next] "m"(the_kernel->context.next->stack)
                     : // clobber
                     "memory");
      }

      void pendsv_handler() __attribute__ ((always_inline)) {
        context.current->stack = ContextSwitchFrame::save();
        ContextSwitchFrame::resume(context.next->stack);
      }

      static void svc_handler() __attribute__ ((always_inline)) {
        register CortexExceptionFrame *frame asm("r2");

        // figure out which stack contains the SVC arguments
        asm volatile ("tst lr, #4                  \n"
                      "ite eq                      \n"
                      "mrseq %[frame], msp         \n"
                      "mrsne %[frame], psp         \n"
                      : // output
                        [frame] "=r"(frame)
                      : // input
                      : // clobber
                        "cc"
                      );
        
        the_kernel->svc(reinterpret_cast<uint8_t *>(frame->pc)[-2], frame);
      }

      void setup() {
        asm volatile ("bkpt #0 \n"
                      "movs r0, #123\n"
                      "msr psp, r0 \n"
                      "msr msp, r0 \n"
                      : // output
                      : // input
                      : // clobber
                        "r0"
                      );
        NVIC_SetPriority(PendSV_IRQn, 0xff);
        __set_CONTROL(control_value(UNPRIVILEGED_THREADS, EXC_PSP, false));
        __ISB();

        context.current = threads[0];
        __set_PSP(reinterpret_cast<uint32_t>(context.current->stack));
        ContextSwitchFrame::resume(context.current->stack);
      }

      void add_thread(Thread &t) {
        for (unsigned i=0; i < countof(threads); ++i) {
          if (threads[i] == 0) {
            threads[i] = &t;
            break;
          }
        }
      }
    };
    
  }
}
