// -*- Mode:C++ -*-

#pragma once

namespace akt {
  template<class T>
  class StackBase {
    const unsigned size;
    T *const elements;
    unsigned tos; // top of stack

  public:


    void               reset()       { tos = 0; }
    bool               empty() const { return tos == 0; }
    bool                full() const { return tos == size; }
    unsigned space_remaining() const { return size - tos; }
    unsigned           depth() const { return tos; }
    T     &operator[](int idx) const { return elements[tos - (idx + 1)]; }
    T                   &top()       { return elements[tos - 1]; }

    StackBase(unsigned size, T *elements) :
      size(size),
      elements(elements),
      tos(0)
    {
    }

    bool push(const T &element) {
      if (full()) return false;

      elements[tos++] = element;
      return true;
    }

    bool pop(T &element) {
      if (empty()) return false;

      element = elements[--tos];
      return true;
    }
  };

  template<class T, unsigned N>
  class Stack : public StackBase<T> {
    T storage[N];

  public:
    Stack() : StackBase<T>(N, storage) { }
  };
}
