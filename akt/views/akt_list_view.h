#pragma once

#include "akt/views/views.h"

namespace akt {
  namespace views {
    enum {
      NO_ITEM = -1
    };

    /**
     * ListViewHelper draws a vertical list of one-line items. The
     * recalculate_frame_size() method should be called to update the frame Rect
     * whenever the contents of the list is updated. Typically, a ListViewHelper
     * will only be created as a subview of ListView which clips the drawing to
     * the visible portion.
     */
    class ListViewHelper : public View {
    public:
      enum {GAP = 1};

      FontBase *font;
      const char **items;
      unsigned item_count;
      int selected_item;

      ListViewHelper();

      virtual void draw_self(akt::views::Canvas &c);
      void recalculate_frame_size();
      coord index_to_lower_coordinate(unsigned int index);
      coord index_to_upper_coordinate(unsigned int index);

    };

    class ListView : public View {
      ListViewHelper helper;
      Point helper_offset;
    public:
      ListView();

      virtual void set_frame(const Rect &r);
      void set_items(const char **items, unsigned count);
      int get_selected_item() const;
      bool get_selected_item_name(const char *&item);
      void set_selected_item(int index);
      void set_font(FontBase &f);
      FontBase &get_font() const;
      void scroll_y_direction(int number_of_pixels);
      void scroll_x_direction(int number_of_pixels);
    };
  };
};
