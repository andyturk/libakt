#include "akt/views/akt_list_view.h"

using namespace akt::views;

ListViewHelper::ListViewHelper() :
    font(0),
    item_count(0),
    selected_item(NO_ITEM)
{
}

void ListViewHelper::recalculate_frame_size() {
    Size frame_size;

    if (font) {
        for (unsigned i=0; i < item_count; ++i) {
            Size item_size(font->measure(items[i]));

            frame_size.w  = std::max(frame_size.w, item_size.w);
            frame_size.h += GAP;
            frame_size.h += item_size.h;
        }

        frame_size.h -= GAP; // no gap after the last element
        frame_size.w += font->size.h/2; // room for the selection marker
    }

    frame.max = frame.min + frame_size;
}

void ListViewHelper::draw_self(Canvas &c) {
    if (font == 0) return;

    coord mark_width = font->size.h/2;
    Point p(frame.min.x + mark_width, frame.min.y);

    for (int i=0; i < (int) item_count; ++i) {
        c.draw_string(p, items[i], *font, 1);

        if (i == selected_item) {
            c.fill_rect(Rect(p.x - mark_width, p.y,
                             font->size.h/2, font->size.h), 1);
        }

        p.y += font->size.h + GAP;
    }
}

coord ListViewHelper::index_to_lower_coordinate(unsigned int index) {
    return (index + 1)*(font->size.h + GAP);
}

coord ListViewHelper::index_to_upper_coordinate(unsigned int index) {
    return index *(font->size.h + GAP);
}

ListView::ListView():
helper_offset(0, 0) {
    add_subview(helper);
}

void ListView::set_items(const char **items, unsigned count) {
    helper.items = items;
    helper.item_count = count;
    helper.selected_item = NO_ITEM;

    helper.recalculate_frame_size();
}

void ListView::set_frame(const Rect &r) {
    View::set_frame(r);
    helper.frame.min = frame.min;
    helper.recalculate_frame_size();
    invalidate(frame);
}

int ListView::get_selected_item() const {
    return helper.selected_item;
}

bool ListView::get_selected_item_name(const char *&item) {
    int index = get_selected_item();

    if (index == NO_ITEM){
        return false;
    }
    item = helper.items[index];
    return true;
}

void ListView::set_selected_item(int index) {
    if (index >= 0 && index < (int) helper.item_count) {
        helper.selected_item = index;
        // scroll display if necessary
        coord helper_index_y_lower = helper.index_to_lower_coordinate(index);
        coord helper_index_y_upper = helper.index_to_upper_coordinate(index);
        if (helper_index_y_upper < helper_offset.y) {
            scroll_y_direction(helper_index_y_upper - helper_offset.y);
        }
        else if (helper_index_y_lower > helper_offset.y + frame.height()) {
            scroll_y_direction(helper_index_y_lower - (helper_offset.y + frame.height()));
        }
    } else {
        // no change
        //helper.selected_item = NO_ITEM;
    }
    invalidate(frame);
}

void ListView::set_font(FontBase &f) {
    helper.font = &f;
    helper.recalculate_frame_size();
    invalidate(frame);
}

void ListView::scroll_y_direction(int number_of_pixels) {
    if (helper.frame.height() <= frame.height()) {
        return;
    }
    helper_offset.y += number_of_pixels;
    if (helper_offset.y < 0) {
        helper_offset.y = 0;
    }
    else if (helper_offset.y + frame.height() > helper.frame.height()) {
        helper_offset.y = helper.frame.height() - frame.height();
    }
    helper.frame.min.y = -helper_offset.y + frame.min.y;
    helper.frame.max.y = helper.frame.min.y + helper.frame.height();
}

void ListView::scroll_x_direction(int number_of_pixels) {
    if (helper.frame.width() <= frame.width()) {
        return;
    }
    helper_offset.x += number_of_pixels;
    if (helper_offset.x < 0) {
        helper_offset.x = 0;
    }
    else if (helper_offset.x + frame.width() > helper.frame.width()) {
        helper_offset.x = helper.frame.width() - frame.width();
    }
    helper.frame.min.x = -helper_offset.x + frame.min.x;
    helper.frame.max.x = helper.frame.min.x + helper.frame.width();
}
